module.exports = function(grunt) {
  //require('jit-grunt')(grunt);

  grunt.initConfig({
    paths: {
      public: "web/typo3conf/ext/jgrp_site/Resources/Public/",
      private: "web/typo3conf/ext/jgrp_site/Resources/Private/",
      base_public: "web/typo3conf/ext/jgrp_base/Resources/Public/",
      base_private: "web/typo3conf/ext/jgrp_base/Resources/Private/",
      sass: "<%= paths.private %>Sass/",
      fonts: "<%= paths.public %>fonts/",
    },
    uglify: {
      js: {
        files: [{
          expand: true,
          cwd: '<%= paths.private %>JavaScript/',
          src: '**/*.js',
          dest: '<%= paths.public %>JavaScript/',
        }],
      }
    },

    watch: {
        sass: {
          files: ['<%= paths.private %>Sass/**/*.scss', '<%= paths.base_private %>Sass/**/*.scss'],
          tasks: 'sass',
        },
        js: {
          files: ['<%= paths.private %>JavaScript/**/*.js','<%= paths.base_private %>JavaScript/**/*.js'],
          tasks: 'uglify',
        }
    },

    clean: {
      css:   '<%= paths.public %>Css/**/*.css',
      fonts: '<%= paths.public %>Fonts/*',
      js:    '<%= paths.public %>JavaScript/**/*.js',
    },

    sass: {
      dist: {
        options: {
          style:'compressed' ,
          loadPath: 'node_modules'
        },
        files: [{
          expand: true,
          cwd: '<%= paths.private %>Sass/',
          src: ['*.scss'],
          dest: '<%= paths.public %>Css/',
          ext: '.css'
        },
        {
          expand: true,
          cwd: '<%= paths.base_private %>Sass/',
          src: ['*.scss'],
          dest: '<%= paths.base_public %>Css/',
          ext: '.css'
        }]
      }
    },

    npmcopy: {
      fonts : {
        files: {
          '<%= paths.public %>Fonts/material-design-icons': 'material-design-icons/iconfont/*',
        },
      },
      css: {
        files : {
          '<%= paths.public %>Css/': 'material-design-lite/material.min.css',
        },
      },
      javascript: {
        options: {
          destPrefix: '<%= paths.public %>JavaScript/Lib/',
        },
        files: {
          'material.min.js': 'material-design-lite/material.min.js',
          'jquery.min.js': 'jquery/dist/jquery.min.js',
          'magnific-popup.min.js': 'magnific-popup/dist/jquery.magnific-popup.min.js',
        },
      },
      fonts_site: {
        options: {
          srcPrefix: '<%= paths.private %>Fonts/'
        },
        files: {
          '<%= paths.public %>Fonts/icomoon': 'IcoMoon/**.{eot,svg,ttf,woff,woff2}',
        }
      }

    }
  });


  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-npmcopy');

  grunt.registerTask("build", [
    "clean", "npmcopy" , "sass", "uglify"
  ]);
  grunt.registerTask("default", [
    "build"
  ]);
};
