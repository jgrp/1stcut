define(["magnificpopup"], function() {

/**
  * init lightbox
  */

    var magnificPopup_config = {
      type:'image',
    image: {
        cursor: ''
    },
    zoom: {
        enabled: true,
        // animation of material design standard curve
        duration: 170,
        easing: 'cubic-bezier(0.4, 0, 0.2, 1)',
      },
      callbacks: {
       open: function() {
        $(".mdl-layout").addClass('blurred-content');
       },
       close: function() {
        $(".mdl-layout").removeClass('blurred-content');
       }
     }
    };

    $('a[rel*="lightbox"]').magnificPopup(magnificPopup_config);

});
