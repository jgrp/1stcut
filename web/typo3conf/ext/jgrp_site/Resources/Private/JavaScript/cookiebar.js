$(function() {

  var setAllowTracking = function () {
    // Set cookie for TypoScript condition
    var exdays = 356;
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = "allow-tracking=true;" + expires + ";path=/";

    // Set localstorage for JavaScript condition
    window.localStorage.setItem('allow-tracking', 'true');
  }


  window.cookieconsent.initialise({
    "position": "bottom-left",
    "type": "opt-in",
    content: {
      message: cookiebar_message,
      allow: cookiebar_allow,
      // deny: cookiebar_deny,
      link: cookiebar_linktext,
      href: cookiebar_link
    },
    elements: {
      // header: '<span class="cc-header">{{header}}</span>&nbsp;',
      // message: '<span id="cookieconsent:desc" class="cc-message">{{message}}</span>',
      // messagelink: '<span id="cookieconsent:desc" class="cc-message">{{message}} <a aria-label="learn more about cookies" tabindex="0" href="{{href}}" target="_blank">{{link}}</a></span>',
      // dismiss: '<a aria-label="dismiss cookie message" tabindex="0" class="cc-btn cc-dismiss btn btn-link btn-sm">{{dismiss}}</a>',
      // allow: '<a aria-label="allow cookies" tabindex="0" class="cc-btn cc-allow mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">{{allow}}</a>',
      // deny: '<a aria-label="deny cookies" tabindex="0" class="cc-btn cc-deny btn btn-link btn-sm">{{deny}}</a>',
      // link: '<a aria-label="learn more about cookies" tabindex="0" href="{{href}}" target="_blank">{{link}}</a>',
      // close: '<span aria-label="dismiss cookie message" tabindex="0" class="cc-close">{{close}}</span>',
    },
    "palette": {
      "popup": {
        "background": "rgba(0,0,0,0.85)"
      },
      "button": {
        "background": "#70000d",
        "text": "#fff"
      }
    },
    compliance: {
      'opt-in': '<div class="cc-compliance cc-highlight"><span class="btn-link"></span>{{allow}}</div>'
    },
    cookie: {
      name: 'jgrp-cookie-status',
    },
    // revokable: false,
    // revokeBtn: '<div class="cc-revoke {{classes}}"></div>',
    onStatusChange: function (status, chosenBefore) {
      // if user accept cookies than trigger event
      if (this.hasConsented()) $(document).trigger('cookies-accepted');
    }
  });



  // get pathname out of location, location.pathname only contains speaking path (no paramaters like id)
  var href = window.location.href;
  var pathname = href.replace(window.location.origin, "");

  // if event triggered, include tracking
  $(document).on('cookies-accepted', function() {

    // if tracking is not included and location is not the dataprivacy page
    if (!window.localStorage.getItem('allow-tracking') && pathname != cookiebar_link ) {


      if (trackingTemplate) {
        // Replace 'EXT:' with real path
        trackingTemplate = trackingTemplate.replace(/^EXT:+/, "/typo3conf/ext/");
        $.get(trackingTemplate, function (data) {
          $('head').append(data);
        });
      }

      // set cookie for loading scripts on page load.
      setAllowTracking();
    }
  });


  $('.mdl-layout').one('scroll', function(e) {
    $(document).trigger('cookies-accepted');
  });

  $('header, section, main, footer').one('click', function() {
    $(document).trigger('cookies-accepted');
  });

});
