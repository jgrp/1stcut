requirejs.config({
  paths: {
    jquery: [
      "https://code.jquery.com/jquery-3.2.1.min"
    ],
    vegasjs: [
      "/typo3conf/ext/jgrp_site/Resources/Public/JavaScript/Lib/vegas"
    ],
    magnificpopup: [
      "/typo3conf/ext/jgrp_site/Resources/Public/JavaScript/Lib/magnific-popup.min"
    ],
    "cookiebar-lib" : [
      "https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min"
    ],
    cookiebar: [
      "/typo3conf/ext/jgrp_site/Resources/Public/JavaScript/cookiebar"
    ]
  },
  shim: {
    vegasjs: ["jquery"],
    magnificpopup: ["jquery"],
    cookiebar: ['cookiebar-lib', 'jquery']
  },
  // Prevent RequireJS from Caching Required Scripts
  urlArgs: "bust=v3"
});


require(["hero", "lightbox", "cookiebar"]);
