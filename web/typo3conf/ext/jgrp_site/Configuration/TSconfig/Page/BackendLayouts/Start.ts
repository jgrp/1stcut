mod {
  web_layout {
    BackendLayouts {
      Start {
        title = Start

        config {
          backend_layout {
            colCount = 1
            rowCount = 6

            rows {
              1 {
                columns {
                  1 {
                    name = Showcase
                    colPos = 2
                  }
                }
              }
              # 2 {
              #   columns {
              #     1 {
              #       name = Crew
              #       colPos = 4

              #       allowed {
              #         CType = textmedia
              #       }
              #     }
              #   }
              # }
              3 {
                columns {
                  1 {
                    name = Content
                    colPos = 0
                  }
                }
              }
              4 {
                columns {
                  1 {
                    name = 3 columns
                    colPos = 7
                  }
                }
              }
              5 {
                columns {
                  1 {
                    name = Awards
                    colPos = 5
                  }
                }
              }
              6 {
                columns {
                  1 {
                    name = Content bottom
                    colPos = 6
                  }
                }
              }
              7 {
                columns {
                  1 {
                    name = Call to Action
                    colPos = 3
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
