plugin.tx_news {
	view {
		# cat=plugin.tx_news/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:jgrp_site/Resources/Private/Templates/Extensions/News
		# cat=plugin.tx_news/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:jgrp_site/Resources/Private/Partials/Extensions/News
		# cat=plugin.tx_news/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:jgrp_site/Resources/Private/Layouts/Extensions/News
	}

	settings {
		# cat=plugin.tx_news/file; type=string; label=Path to CSS file
		cssFile >
	}
}
