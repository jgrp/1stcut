lib {

  heroContent < lib.content
  heroContent.select.where = colPos=2

  callToAction < lib.content
  callToAction.select.where = colPos=3

  crew < lib.content
  crew.select.where = colPos=4

  contentBottom < lib.content
  contentBottom.select.where = colPos=6

  awards < lib.content
  awards.select.where = colPos=5

  threeCol < lib.content
  threeCol.select.where = colPos=7


  page {
    template {
      layoutRootPaths {
        20 = EXT:jgrp_site/Resources/Private/Layouts/Page
      }

      partialRootPaths {
        20 = EXT:jgrp_site/Resources/Private/Partials/Page
      }

      templateRootPaths {
        20 = EXT:jgrp_site/Resources/Private/Templates/Page
      }

      variables {
        heroContent =< lib.heroContent
        crew =< lib.crew
        callToAction =< lib.callToAction
        contentBottom =< lib.contentBottom
        awards =< lib.awards
        threeCol=< lib.threeCol
      }

    }

  }
}
