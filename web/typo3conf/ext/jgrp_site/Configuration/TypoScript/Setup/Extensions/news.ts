plugin.tx_news {
  settings {
    #Displays a dummy image if the news have no media items
    displayDummyIfNoMedia = 0

    list {
      # media configuration
      media {
        image {
          maxWidth = 450
          maxHeight = 350
        }
      }
    }

  }
}
