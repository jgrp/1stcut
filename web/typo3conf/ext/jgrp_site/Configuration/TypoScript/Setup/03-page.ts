page {

  meta {
    robots = noindex, nofollow
  }

  includeCSS {
    main = EXT:jgrp_site/Resources/Public/Css/main.css
    cookieconsent = https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css
    cookieconsent.external = 1
  }

  headerData.1518436.settings.language.1 = en

  footerData {

    2 = FLUIDTEMPLATE
    2 {
      file = EXT:jgrp_site/Resources/Private/Templates/Assets/JavaScriptConfig.html
      variables {
        dataprivacyPage = TEXT
        dataprivacyPage.value = {$lib.cookiebar.dataprivacyPage}

        trackingTemplate = TEXT
        trackingTemplate.value = {$lib.tracking.template}
      }

      #    extbase.controllerExtensionName = JgrpSite
    }

    # Not using includeJS here since we need "data-main"
    3 = FLUIDTEMPLATE
    3 {
      file = EXT:jgrp_site/Resources/Private/Templates/Assets/JavaScript.html
      extbase.controllerExtensionName = JgrpSite
    }

  }

  10 {
  }

}

    # only allow tracking if user allows over cookiebar
[globalString = _COOKIE|allow-tracking = true][globalString = _COOKIE|jgrp-cookie-status = allow]

  page {
    headerData {
      1537957 = FILE
      1537957.file = {$lib.tracking.template}
    }
  }

[global]


