<?php

$EM_CONF[$_EXTKEY] = [
  'title' => 'Site Resources',
  'description' => 'Functionality and resources for the website.',
  'category' => 'misc',
  'state' => 'stable',
  'version' => '1.0.0',
  'depends' => [
  ],
];
