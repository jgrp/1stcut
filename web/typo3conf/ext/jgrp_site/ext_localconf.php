<?php
defined('TYPO3_MODE') or die();

$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['base_rte'] = 'EXT:jgrp_site/Configuration/RTE/base_rte.yaml';


$TYPO3_CONF_VARS['SYS']['locallangXMLOverride']['default']['EXT:news/Resources/Private/Language/locallang.xlf'][] = 'EXT:jgrp_site/Resources/Private/Language/Extensions/News/locallang.xlf';
$TYPO3_CONF_VARS['SYS']['locallangXMLOverride']['de']['EXT:news/Resources/Private/Language/locallang.xlf'][] = 'EXT:jgrp_site/Resources/Private/Language/Extensions/News/de.locallang.xlf';


$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/db_layout.php']['drawHeaderHook'][1490965620] = \Jgrp\JgrpSite\Hooks\PageLayoutControllerHook::class . '->addBackendCss';
