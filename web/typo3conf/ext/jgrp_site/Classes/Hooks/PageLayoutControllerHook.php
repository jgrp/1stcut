<?php
namespace Jgrp\JgrpSite\Hooks;


use TYPO3\CMS\Backend\Controller\PageLayoutController;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Hook for the backend controller
 */
class PageLayoutControllerHook {

    /**
     * @param array $parameters
     * @param PageLayoutController $backendController
     * @return void
     */
    public function addBackendCss(array $parameters, PageLayoutController $backendController)
    {
        /** @var PageRenderer */
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $pageRenderer->addCssFile('EXT:jgrp_site/Resources/Public/Css/backend.css');

        return '';
    }
}
