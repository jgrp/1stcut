<?php

  unlink("index.php");
  unlink("typo3");

  echo file_exists('../vendor/typo3/cms/index.php');
  echo file_exists('../vendor/typo3/cms/typo3/');

// www/htdocs/w00e1f45/1stcut/vendor/typo3/cms/index.php
  symlink("../vendor/typo3/cms/index.php", "index.php");
  symlink("../vendor/typo3/cms/typo3/", "typo3");

  echo ( "Symlinks created: " );
  echo ( readlink("index.php") . ", ");
  echo ( readlink("typo3") );

?>
