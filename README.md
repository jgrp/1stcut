# jgrp base

## Install Sass

First update Ruby to > 2
https://jibai31.wordpress.com/2013/09/01/upgrading-your-vagrant-box-to-ruby-20/

then install sass with `gem install sass`
> https://www.npmjs.com/package/grunt-contrib-sass

Alternativ:
`sudo apt-get install ruby-compass`

### Error: 

*Invalid CSS after "@charset ": expected string, was "UTF-8;"*
`export LC_ALL=en_US.UTF-8` and/or `export LANG=en_US.UTF-8`


## RealURL configuration

A minimal default configuration for [RealURL](https://typo3.org/extensions/repository/view/realurl) with sensible defaults can be included in `realurl_conf.php`:

```php
require __DIR__ . '/ext/jgrp_base/Configuration/RealURL/Default.php';
```

### RealURL News configuration

To enable the RealURL configuration for the [News](https://typo3.org/extensions/repository/view/news) extension add a line like this to `realurl_conf.php`:

```php
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars'][<news-pid>] = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['_DEFAULT']['fixedPostVars']['_NEWS'];
```

Here `<news-pid>` must be filled with the PID where the news plugin is located. The default setup assumes that news details are displayed on the same page and should simply extend the news list URL.


